1. CREATE DATABASE battlefield;
2. use battlefield;
3. CREATE TABLE reports(id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT, ammunition SMALLINT UNSIGNED NOT NULL,
                        soldiers SMALLINT UNSIGNED NOT NULL, duration DOUBLE(6,1) UNSIGNED NOT NULL, PRIMARY KEY(id))
                        engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
(Didn't see the next page at first, so added new columns with the following queries)
4. ALTER TABLE reports ADD COLUMN critique TINYTEXT;
5. ALTER TABLE reports ADD COLUMN posted TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;