<?php
$ammo = $_POST['ammo'];
$soldiers = $_POST['soldiers'];
$duration = $_POST['duration'];
$critique = $_POST['critique'];
if (empty($_POST)) {
    die("Data submission error, please try again");
}
$stmt = $mysqli->prepare("INSERT INTO reports ammunition, soldiers, duration, critique VALUES ?,?,?,?");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('iids', $ammo, $soldiers, $duration, $critique);
    $stmt->execute();
    $stmt->close();
    if (mysql_error()) {
        die ("Data entry failed, please try again");
    }
    else {
        header("Location: battlefield-submit.html");
    }
?>