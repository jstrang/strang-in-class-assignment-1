<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1{
    text-align: center;
}
</style>
</head>
<body><div id="main">
 <h1>Battlefield Analysis</h1>
 <h2>Latest Critiques</h2>
 <?php
require 'database.php';
 
$stmt = $mysqli->prepare("select critique from reports order by posted");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->execute();
 
$stmt->bind_result($report);
 
echo "<ul>\n";
for($i=0; $i<5; $i++){
	$stmt->fetch();
        printf("\t<li>%s</li>\n",
		htmlspecialchars( $report )
	);
}
echo "</ul>\n";
 
$stmt->close();
?>
 <h2>Battle Statistics</h2>
 <?php
 $stmt = $mysqli->prepare("select soldiers, AVG(ammunition/duration) as ammo_per_sec from reports group by soldiers order by soldiers descending");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
$stmt->bind_result($soldiers, $ammoPer);
echo "<table border=\"1\">";
echo "<tr>
<th>Number of Soldiers</th>
<th>Pounds of Ammunition per Second</th>
</tr>";

while($stmt->fetch()){
    echo "<tr>
    $soldiers</td>
<td>$ammoPer</td>
</tr>";
}
echo "</table>";

 ?>
 <a href="battlefield-submit.html">Submit a New Battle Report</a>
</div></body>
</html>